import React, {Component} from 'react';
import {TextInput, View, Text} from 'react-native';
import styles from './styles'
import Icon from 'react-native-vector-icons/dist/FontAwesome';

class PeopleItem extends Component {

    constructor(props){
        super(props);
        this.state = {
        };
    }
    render() {
        return (
            <View style={[styles.container, {borderBottomColor: this.props.border ? 'rgb(237, 237, 237)' : 'transparent', borderBottomWidth: this.props.border ? 1 : 0}]}>
                <Text style={styles.name}>اعتبار کسب شده: {this.props.item.creadit} ریال</Text>
                <Text style={styles.name}>{this.props.item.lname} {this.props.item.fname}</Text>
            </View>
        );
    }
}
export default PeopleItem;