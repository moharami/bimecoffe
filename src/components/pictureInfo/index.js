import React, {Component} from 'react';
import {View, Text, TouchableOpacity, Image, BackHandler} from 'react-native';
import LIcon from 'react-native-vector-icons/dist/SimpleLineIcons';
import FIcon from 'react-native-vector-icons/dist/Feather';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import pic1 from '../../assets/addImage/pic1.png'
import pic2 from '../../assets/addImage/pic2.png'
import pic3 from '../../assets/addImage/pic3.png'
import ImagePicker from 'react-native-image-picker'
import AlertView from '../../components/modalMassage'
import PictureModal from './pictureModal'

class PictureInfo extends Component {
    constructor(props){
        super(props);
        this.state = {
            imgSrc: '',
            modalVisible: false,
            modalVisible2: false
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress(){
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    selectPhotoTapped() {
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true
            }
        };

        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response pics  = ', response);
            console.log('Response pics file size = ', response.fileSize);

            if (response.didCancel) {
                console.log('User cancelled photo picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                if(response.fileSize > 10048576) {
                    this.setState({modalVisible: true})
                }
                else{
                    let source = { uri: response.uri };
                    this.setState({
                        imgSrc: response.uri
                    })
                    this.props.pickerImage(response.uri);
                }
            }
        });
    }
    closeModal() {
        this.setState({modalVisible: false});
    }
    closeModal2() {
        this.setState({modalVisible2: false});
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.left}>
                    {
                        this.state.imgSrc !== '' ?
                            <View style={{flexDirection: 'row'}}>
                                <TouchableOpacity onPress={() => this.setState({edit: false})} style={styles.iconRightEditContainer}>
                                    <FIcon name="check" size={18} color="white" />
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.setState({imgSrc: ''})}>
                                    <Icon name="close" size={18} color="red" style={{paddingLeft: 10, paddingTop: 5}} />
                                </TouchableOpacity>
                            </View>
                            :
                            <TouchableOpacity onPress={() => this.selectPhotoTapped()} style={styles.iconRightContainer}>
                                <LIcon name="cloud-upload" size={18} color="white" style={{borderColor: 'white', borderWidth: 1, borderRadius: 20, padding: 2, marginRight: 5}} />
                                <Text style={styles.topLabel}>بارگذاری</Text>
                            </TouchableOpacity>

                    }
                </View>
                <View style={styles.right}>
                    <View>
                        <Text style={styles.amount}>{this.props.label}</Text>
                        {
                            this.props.req ?
                                <Text style={styles.amount}>(الزامی)</Text>
                                    : null
                        }
                    </View>
                    <View style={[styles.imageContainer, {padding: this.state.imgSrc !== '' ? 0 : 20}]}>
                        {
                            this.state.imgSrc !== '' ?
                                <TouchableOpacity onPress={() => this.setState({modalVisible2: true})}>
                                    <Image source={{uri: this.state.imgSrc}} style={{width: 60, height: 60, borderRadius: 60}} />
                                </TouchableOpacity>
                                        :
                                <Image source={this.props.pic === 'pic1' ? pic1 : (this.props.pic === 'pic2' ? pic2 : pic3)} style={styles.bodyImage} />
                        }

                    </View>
                </View>
                <View style={{position: 'absolute', zIndex: -1}}>
                    <AlertView
                        closeModal={(title) => this.closeModal(title)}
                        modalVisible={this.state.modalVisible}
                        onChange={() => this.setState({modalVisible: false})}
                        title='سایز عکس نباید بیشتر از 10 مگابایت باشد'
                    />
                    <PictureModal
                        closeModal={(title) => this.closeModal2(title)}
                        modalVisible={this.state.modalVisible2}
                        imgSrc={this.state.imgSrc}
                        title='سایز عکس نباید بیشتر از 10 مگابایت باشد'
                    />

                </View>
            </View>
        );
    }
}
export default PictureInfo;