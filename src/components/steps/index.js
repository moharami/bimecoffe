import React from 'react';
import {Text, View, InteractionManager, ScrollView} from 'react-native';

import styles from './styles'
export default class Steps extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            active: 0
        };
    }
    // componentDidMount() {
    //     // this.myScroll.scrollTo({x: 300, y: 0, animated: true})
    //     // console.log('this.myScroll',this.myScroll)
    //     this.props.active === 1 ? this.myScroll.scrollTo({x: 300, y: 0, animated: true}) : null;
    //     // setTimeout(() => {
    //     //     this.myScroll.scrollTo({y: 100})
    //     // }, 1)
    //     console.log("called DidMount");
    // }
    // componentDidMount() {
    //     this.forceUpdate();
    //
    //     console.log('something here i nstrp did mount')
    //     setTimeout(() => {
    //         // this.myScroll.scrollTo({x: 350, y: 0})
    //         console.log('this.props.accccccccccccccctibe , ', this.props.active)
    //         switch (this.props.active) {
    //             case 1:
    //                 console.log('in active 1', this.props.active)
    //                 break;
    //             case 2:
    //                 console.log('in active 2', this.props.active)
    //                 break;
    //             case 3:
    //                 this.setState({active: 3}, () => {  console.log('in active 2', this.props.active)
    //                     console.log('thisssss.scrollview', this.myScroll)
    //                     this.myScroll.scrollTo({x: 200, y: 0});});
    //
    //
    //
    //                 break;
    //             case 4:
    //                 this.setState({active: 3}, () => {  console.log('in active 2', this.props.active)
    //                     console.log('thisssss.scrollview', this.myScroll)
    //                     this.myScroll.scrollTo({x: 350, y: 0});
    //                 });
    //                 break;
    //             case 5:
    //
    //                 this.setState({active: 3}, () => {  console.log('in active 2', this.props.active)
    //                     console.log('thisssss.scrollview', this.myScroll)
    //                     this.myScroll.scrollTo({x: 400, y: 0});
    //                 });
    //
    //                 break;
    //             case 6:
    //                 this.setState({active: 3}, () => {  console.log('in active 2', this.props.active)
    //                     console.log('thisssss.scrollview', this.myScroll)
    //                     this.myScroll.scrollTo({x: 500, y: 0});
    //                 });
    //
    //                 break;
    //             case 7:
    //                 this.setState({active: 3}, () => {  console.log('in active 2', this.props.active)
    //                     console.log('thisssss.scrollview', this.myScroll)
    //                     this.myScroll.scrollTo({x: 600, y: 0});
    //                 });
    //                 break;
    //             default:
    //                 break;
    //         }
    //     }, 100)
    // }
    // shouldComponentUpdate(){
    //     console.log('shouldComponentUpdate is running')
    //     this.forceUpdate();
    // }
    render(){
        return (
            <ScrollView ref={(select) => this.myScroll = select}  style={{ transform: [
                { scaleX: -1}

            ],}}
                        horizontal={true} showsHorizontalScrollIndicator={false}
                        // onContentSizeChange={() => this._onContentSizeChange()}
            >
                <View style={styles.container}>
                    <View style={styles.navContainer}>
                        <Text style={[styles.label, {color: this.props.active === 7 ? 'rgba(216, 154, 0, 1)': 'white'}]}>پرداخت</Text>
                        <View style={[styles.numContainer, { backgroundColor: this.props.active === 7 ? 'rgba(255, 193, 39, 1)' : 'white' }]}>
                            <Text style={[styles.text, {color: this.props.active === 7 ? 'white': 'rgba(122, 130, 153, 1)'}]}>7</Text>
                        </View>
                    </View>
                    <View style={styles.navContainer}>
                        <Text style={styles.seprator}>/</Text>
                        <Text style={[styles.label, {color: this.props.active === 6 ? 'rgba(216, 154, 0, 1)': 'white'}]}>تایید نهایی</Text>
                        <View style={[styles.numContainer, { backgroundColor: this.props.active === 6 ? 'rgba(255, 193, 39, 1)' : 'white' }]}>
                            <Text style={[styles.text, {color: this.props.active === 6 ? 'white': 'rgba(122, 130, 153, 1)'}]}>6</Text>
                        </View>
                    </View>
                    <View style={styles.navContainer}>
                        <Text style={styles.seprator}>/</Text>
                        <Text style={[styles.label, {color: this.props.active === 5 ? 'rgba(216, 154, 0, 1)': 'white'}]}>انتخاب زمان</Text>
                        <View style={[styles.numContainer, { backgroundColor: this.props.active === 5 ? 'rgba(255, 193, 39, 1)' : 'white' }]}>
                            <Text style={[styles.text, {color: this.props.active === 5 ? 'white': 'rgba(122, 130, 153, 1)'}]}>5</Text>
                        </View>
                    </View>
                    <View style={styles.navContainer}>
                        <Text style={styles.seprator}>/</Text>
                        <Text style={[styles.label, {color: this.props.active === 4 ? 'rgba(216, 154, 0, 1)': 'white'}]}>بارگذاری تصاویر</Text>
                        <View style={[styles.numContainer, { backgroundColor: this.props.active === 4 ? 'rgba(255, 193, 39, 1)' : 'white' }]}>
                            <Text style={[styles.text, {color: this.props.active === 4 ? 'white': 'rgba(122, 130, 153, 1)'}]}>4</Text>
                        </View>
                    </View>
                    <View style={styles.navContainer}>
                        <Text style={styles.seprator}>/</Text>
                        <Text style={[styles.label, {color: this.props.active === 3 ? 'rgba(216, 154, 0, 1)': 'white'}]}>مشخصات خریدار</Text>
                        <View style={[styles.numContainer, { backgroundColor: this.props.active === 3 ? 'rgba(255, 193, 39, 1)' : 'white' }]}>
                            <Text style={[styles.text, {color: this.props.active === 3 ? 'white': 'rgba(122, 130, 153, 1)'}]}>3</Text>
                        </View>
                    </View>
                    <View style={styles.navContainer}>
                        <Text style={styles.seprator}>/</Text>
                        <Text style={[styles.label, {color: this.props.active === 2 ? 'rgba(216, 154, 0, 1)': 'white'}]}>قیمت ها</Text>
                        <View style={[styles.numContainer, { backgroundColor: this.props.active === 2 ? 'rgba(255, 193, 39, 1)' : 'white' }]}>
                            <Text style={[styles.text, {color: this.props.active === 2 ? 'white': 'rgba(122, 130, 153, 1)'}]}>2</Text>
                        </View>
                    </View>
                    <View style={styles.navContainer}>
                        <Text style={styles.seprator}>/</Text>
                        <Text style={[styles.label, {color: this.props.active === 1 ? 'rgba(216, 154, 0, 1)': 'white'}]}>مشخصات</Text>
                        <View style={[styles.numContainer, { backgroundColor: this.props.active === 1 ? 'rgba(255, 193, 39, 1)' : 'white' }]}>
                            <Text style={[styles.text, {color: this.props.active === 1 ? 'white': 'rgba(122, 130, 153, 1)'}]}>1</Text>
                        </View>
                    </View>
                </View>
            </ScrollView>
        );
    }
}