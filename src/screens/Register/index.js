import React, {Component} from 'react';
import {
    View,
    TouchableOpacity,
    Text,
    TextInput,
    KeyboardAvoidingView,
    Image,
    AsyncStorage,
    BackHandler,
    Picker
} from 'react-native';
import styles from './styles'
import Icon from 'react-native-vector-icons/FontAwesome';
import FIcon from 'react-native-vector-icons/Feather';
import {Actions} from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';
import Axios from 'axios'
// import ScalingDrawer from '../../components/react-native-scaling-drawer'
import PersianCalendarPicker from 'react-native-persian-calendar-picker';
export const url = 'http://bimecafe.com/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import AlertView from '../../components/modalMassage'
import moment_jalaali from 'moment-jalaali'
import Selectbox from 'react-native-selectbox'
import {connect} from 'react-redux';
import {store} from '../../config/store';

class Register extends Component {
    constructor(props) {
        super(props);
        this.state = {
            text: '',
            loading: false,
            fname: '',
            lname: '',
            email: '',
            mobile: '',
            birthday: '',
            selectedStartDate: null,
            emailCorrect: false,
            showPicker: false,
            signed: false,
            modalVisible: false,
            birthdayYear: {key: 0, label: 'سال تولد', value: 0},
            birthdayMonth: {key: 0, label: 'ماه تولد', value: 0},
            birthdayDay: {key: 0, label: 'روز تولد', value: 0},
            correct: false

        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        if(this.props.cantBack) {
            Actions.insuranceBuy({openDrawer: this.props.openDrawer})
            return true;
        }
        else{
            Actions.pop({refresh: {refresh: Math.random()}});
            return true;
        }
    };
    componentWillMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    closeModal() {
        this.setState({modalVisible: false});
    }
    signUp() {
        if(this.state.fname === '' || this.state.lname === '' || this.state.birthdayYear.value === 0 || this.state.birthdayMonth.value  === 0 || this.state.birthdayDay.value === 0) {
            this.setState({modalVisible: true, correct: true});
        }
        else {
            let Birth = this.state.birthdayYear.value+"/"+this.state.birthdayMonth.value+"/"+this.state.birthdayDay.value;
            console.log('bbbbitrh',moment_jalaali(Birth, 'jYYYY/jMM/jDD').format('YYYY-MM-DD'))
            // let stttr = moment_jalaali(Birth, 'jYYYY/jM/jD').format('YYYY-M-D')
            let newRes = "";
            let stttr = moment_jalaali(Birth, 'jYYYY/jMM/jDD').format('YYYY-MM-DD')
            stttr = stttr.replace(/۰/g, "0");
            stttr = stttr.replace(/۱/g, "1");
            stttr = stttr.replace(/۲/g, "2");
            stttr = stttr.replace(/۳/g, "3");
            stttr = stttr.replace(/۴/g, "4");
            stttr = stttr.replace(/۵/g, "5");
            stttr = stttr.replace(/۶/g, "6");
            stttr = stttr.replace(/۷/g, "7");
            stttr = stttr.replace(/۸/g, "8");
            stttr = stttr.replace(/۹/g, "9");
            console.log('neW STTR ', stttr )

            this.setState({loading: true});
            Axios.post('/set_profile', {
                fname: this.state.fname,
                lname: this.state.lname,
                mobile: this.props.mobile,
                birthday: stttr
            }).then(response=> {
                this.setState({loading: false});
                console.log('signup info', response);
                if(response.data.msg === 'ProfileSuccess'){
                    store.dispatch({type: 'USER_INFO_FETCHED', payload: response.data.data});
                    store.dispatch({type: 'USER_LOGED', payload: true});

                    // Alert.alert('','ثبت نام شما با موفقیت انجام شد');
                    this.setState({signed: true, modalVisible: true, loading: false});

                    AsyncStorage.getItem('token').then((info) => {
                        if(info !== null) {
                            const newInfo = JSON.parse(info);
                            console.log('tokennnn',newInfo);

                            const info2 = {'wasRecentlyCreated': 1};
                            AsyncStorage.mergeItem('token', JSON.stringify(info2));
                            AsyncStorage.getItem('token').then((info) => {
                                if(info !== null) {
                                    const newInfo = JSON.parse(info);
                                    console.log('tokene jadid bade sabte name',newInfo);

                                }
                            })
                        }
                    });



                    if(this.props.insBuy) {
                        Actions.prices({insBuy: true, openDrawer: this.props.openDrawer, factor:this.props.factor, user_details:this.props.user_details, insurType:this.props.insurType, instalment: this.props.instalment, signup: true, birthdayYear: this.state.birthdayYear.value, birthdayMonth: this.state.birthdayMonth.value, birthdayDay: this.state.birthdayDay.value})
                    }
                    else if(this.props.profile) {
                        Actions.profile({openDrawer: this.props.openDrawer, mobile: this.props.mobile})
                    }
                }
                else {
                    Actions.push('login')
                }
            })
            .catch((error) => {
                this.setState({modalVisible: true, loading: false});
            });
        }
    }
    onDateChange(date) {
        setTimeout(() => {this.setState({showPicker: false})}, 200)
        this.setState({ selectedStartDate: date });
    }
    render() {
        let yearArray2 = [], monthArray = [], dayArray = [];
        for(let i=1397; i>=1300 ; i--){
            yearArray2.push({key: i, label: i.toString(), value: i})
        }
        const month = ["01", "02", "03", "04", "05", "06", "07", "08", "09","10", "11","12"]
        monthArray = month.map((item)=> {return {key: parseInt(item), label: item, value: item}})
        const day = ["01", "02", "03", "04", "05", "06", "07", "08", "09","10", "11","12", "13", "14", "15", "16", "17", "18", "19", "20", "21","22", "23","24", "25", "26", "27", "28", "29", "30", "31"];
        dayArray = day.map((item)=> {return {key: parseInt(item), label: item, value: item}})

        return (
            <KeyboardAvoidingView style={styles.container}  behavior="padding" enabled>
                {this.state.loading ? <Loader send={false}/> :

                    <View style={styles.send}>
                        <Image style={{alignSelf: 'flex-end', width: '30%', marginRight: 10}} resizeMode={'contain'}
                               source={require('../../assets/coffebimeLogo.png')}/>
                        <View style={styles.body}>
                            <Text style={styles.header}>خوش آمدید</Text>
                            <Text style={styles.label}>برای بهره مندی از خدمات بیمه کافه لطفا ثبت نام کنید</Text>
                            <View style={{display: 'flex', flexDirection: 'row', marginTop: 20}}>
                                <TextInput
                                    placeholder="نام"
                                    placeholderTextColor={'#C8C8C8'}
                                    underlineColorAndroid='transparent'
                                    value={this.state.fname}
                                    maxLength={15}
                                    style={{
                                        textAlign: 'right',
                                        borderWidth: 1,
                                        borderColor: '#C8C8C8',
                                        height: 40,
                                        backgroundColor: 'white',
                                        paddingRight: 15,
                                        flex: .9,
                                        borderTopLeftRadius: 6,
                                        borderBottomLeftRadius: 6,
                                        borderBottomRightRadius: .1,
                                        borderTopRightRadius: 1,
                                        fontSize: 14,
                                        color: '#7A8299',
                                        fontFamily: 'IRANSansMobile(FaNum)'
                                    }}
                                    onChangeText={(text) => this.setState({fname: text})}/>
                                <View style={{
                                    backgroundColor: '#C8C8C8',
                                    borderTopRightRadius: 6,
                                    borderBottomRightRadius: 6,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    height: 40
                                }}>
                                    <Icon name={'user'} style={{fontSize: 23, paddingRight: 15, paddingLeft: 15}}/>
                                </View>
                            </View>
                            <View style={{display: 'flex', flexDirection: 'row', marginTop: 20}}>
                                <TextInput
                                    placeholder="نام خانوادگی"
                                    maxLength={15}
                                    placeholderTextColor={'#C8C8C8'}
                                    underlineColorAndroid='transparent'
                                    value={this.state.lname}
                                    style={{
                                        textAlign: 'right',
                                        borderWidth: 1,
                                        borderColor: '#C8C8C8',
                                        height: 40,
                                        backgroundColor: 'white',
                                        paddingRight: 15,
                                        flex: .9,
                                        borderTopLeftRadius: 6,
                                        borderBottomLeftRadius: 6,
                                        borderBottomRightRadius: .1,
                                        borderTopRightRadius: 1,
                                        fontSize: 14,
                                        color: '#7A8299',
                                        fontFamily: 'IRANSansMobile(FaNum)'
                                    }}
                                    onChangeText={(text) => this.setState({lname: text})}/>
                                <View style={{
                                    backgroundColor: '#C8C8C8',
                                    borderTopRightRadius: 6,
                                    borderBottomRightRadius: 6,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    height: 40
                                }}>
                                    <Icon name={'user'} style={{fontSize: 23, paddingRight: 15, paddingLeft: 15}}/>
                                </View>
                            </View>
                            <View style={{flexDirection: 'row',  width: '90%', borderRadius: 10, alignItems: 'center', justifyContent: 'space-between', paddingTop: 20, paddingBottom: 20}}>
                                <Selectbox
                                    style={{width: '30%', height: 35, paddingTop: '3%', paddingRight: 10, backgroundColor: 'lightgray', borderRadius: 5}}
                                    selectLabelStyle={{textAlign: 'right', color: 'black', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13, paddingTop: 0}}
                                    optionLabelStyle={{textAlign: 'center', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                    selectedItem={this.state.birthdayYear}
                                    cancelLabel="لغو"
                                    onChange={(itemValue) =>{
                                        this.setState({
                                            birthdayYear: itemValue
                                        })}}
                                    items={yearArray2} />
                                <Selectbox
                                    style={{width: '30%', height: 35, paddingTop: '3%', paddingRight: 10, backgroundColor: 'lightgray', borderRadius: 5}}
                                    selectLabelStyle={{textAlign: 'right', color: 'black', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13, paddingTop: 0}}
                                    optionLabelStyle={{textAlign: 'center', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                    selectedItem={this.state.birthdayMonth}
                                    cancelLabel="لغو"
                                    onChange={(itemValue) =>{
                                        this.setState({
                                            birthdayMonth: itemValue
                                        })}}
                                    items={monthArray} />
                                <Selectbox
                                    style={{width: '30%', height: 35, paddingTop: '3%', paddingRight: 10, backgroundColor: 'lightgray', borderRadius: 5}}
                                    selectLabelStyle={{textAlign: 'right', color: 'black', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13, paddingTop: 0}}
                                    optionLabelStyle={{textAlign: 'center', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                    selectedItem={this.state.birthdayDay}
                                    cancelLabel="لغو"
                                    onChange={(itemValue) =>{
                                        this.setState({
                                            birthdayDay: itemValue
                                        })}}
                                    items={dayArray} />

                            </View>
                            <TouchableOpacity onPress={() => this.signUp()} style={styles.advertise}>
                                <LinearGradient start={{x: 0, y: 1}} end={{x: 1, y: 1}} colors={['#3cd0ef', '#23d1ba']}
                                                style={styles.advertise}>
                                    <View>
                                        <Text style={styles.buttonTitle}>ثبت نام</Text>
                                    </View>
                                </LinearGradient>
                            </TouchableOpacity>
                        </View>
                        {
                            this.state.showPicker ?
                                <View style={{
                                    position: 'absolute',
                                    bottom: '35%',
                                    zIndex: 9999,
                                    backgroundColor: 'white'
                                }}>
                                    <PersianCalendarPicker
                                        onDateChange={(date) => this.onDateChange(date)}
                                    />
                                </View>
                                : null
                        }
                    </View>
                }
                <Image style={{height: '20%', zIndex: -1, width: '100%', position: 'absolute', bottom: 0}}
                       resizeMode={'cover'} source={require('../../assets/company-hero-3.png')}/>
                <AlertView
                    closeModal={(title) => this.closeModal(title)}
                    modalVisible={this.state.modalVisible}
                    onChange={() => this.setState({modalVisible: false})}
                    title={this.state.signed ? 'ثبت نام شما با موفقیت انجام شد' :(this.state.correct ? 'لطفا تمام موارد را پر نمایید' : 'مشکلی در برقراری ارتباط با سرور به وجود آمده لطفا دوباره سعی کنید')}
                />
            </KeyboardAvoidingView>
        );
    }
}

export default Register;