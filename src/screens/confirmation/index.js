import React, {Component} from 'react';
import { View, TouchableOpacity, Text, TextInput, Image, AsyncStorage, BackHandler, PermissionsAndroid  ,KeyboardAvoidingView} from 'react-native';
import styles from './styles'
import Icon from 'react-native-vector-icons/FontAwesome'
import {Actions} from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';
import Axios from 'axios'
;
export const url = 'http://bimecafe.com/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import {store} from '../../config/store';
import {connect} from 'react-redux';
import AlertView from '../../components/modalMassage'
import SmsListener from 'react-native-android-sms-listener'
import TimerCountdown from 'react-native-timer-countdown';

class Confirmation extends Component {

    // secondTextInput=null
    // thirdTextInput=null
    // fourthTextInput=null
    // number=['','','','']

    constructor(props){
        super(props);
        this.SMSReadSubscription = {};

        this.state = {
            text: '',
            text2: '',
            text3: '',
            text4: '',
            loading: false,
            codeDetect: false,
            modalVisible: false,
            code: '',
            timerEnd: false

        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    async requestReadSmsPermission() {
        try {
            var granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.READ_SMS, {
                    title: 'Auto Verification OTP',
                    message: 'need access to read sms, to verify OTP'
                }
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                // alert('READ_SMS permissions granted', granted);
                console.log('READ_SMS permissions granted', granted);
                granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.RECEIVE_SMS, {
                        title: 'Receive SMS',
                        message: 'Need access to receive sms, to verify OTP'
                    }
                );
                if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                    // alert('RECEIVE_SMS permissions granted', granted);
                    console.log('RECEIVE_SMS permissions granted', granted);
                    SmsListener.addListener(message => {
                        // alert(message);
                        console.log(message);
                        //message.body will have the message.
                        //message.originatingAddress will be the address.
                    });
                } else {
                    // alert('RECEIVE_SMS permissions denied');
                    console.log('RECEIVE_SMS permissions denied');
                }
            } else {
                // alert('READ_SMS permissions denied');
                console.log('READ_SMS permissions denied');
            }
        } catch (err) {
            console.log(err);
        }
    }

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress(){
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    componentDidMount() {
        this.requestReadSmsPermission();

        SmsListener.addListener(message => {
            console.info('message',message)
            const str = message.body;
            const newStr = str.match(/\d+/);
            console.log('ne==', newStr[0])

            this.setState({code: newStr[0]}, () => this.confirm());
        })
    }
    confirm() {
        if(this.state.code === '')
            this.setState({codeDetect: true, modalVisible: true});
        else{
            console.log('code',  this.state.code)
            this.setState({loading: true});
            Axios.post('/check_code', {
                mobile: this.props.mobile,
                code:  this.state.code
            }).then(response=> {
                this.setState({
                    loading: false
                }, () => {
                    store.dispatch({type: 'USER_INFO_FETCHED', payload: response.data.data});
                    store.dispatch({type: 'USER_LOGED', payload: true});

                    // store.dispatch({type: 'USER_LOGED', payload: true});
                    console.log('confirm data', response);
                    const info = {'token': response.data.access_token, 'mobile': this.props.mobile, 'code': this.state.text, 'expires_at': response.data.expires_at, 'user_id': response.data.data.id, 'wasRecentlyCreated': response.data.data.wasRecentlyCreated};
                    const newwwwAsync = AsyncStorage.setItem('token', JSON.stringify(info));
                    console.log('newwwwAsync', newwwwAsync);
                    const info2 = {'loged': true};
                        const newwwwAsync2 = AsyncStorage.setItem('loged', JSON.stringify(info2));
                    console.log('newwwwAsync2', newwwwAsync2);
                    AsyncStorage.getItem('loged').then((info) => {
                        if(info !== null) {
                            const newInfo = JSON.parse(info);
                           console.log('neeeew iiiiinfo', newInfo)
                        }
                    })
                    // Actions.home({openDrawer: this.props.openDrawer, loged: true});
                    console.log('profilllllllllle true ? ', this.props.profile);
                    console.log('insbuy true ? ', this.props.insBuy);
                    if(this.props.profile) {
                        if(response.data.data.wasRecentlyCreated === 0) {
                            Actions.register({openDrawer: this.props.openDrawer, mobile: this.props.mobile, profile: true})
                        }
                        else {
                            Actions.profile({openDrawer: this.props.openDrawer, mobile: this.props.mobile})
                        }
                    }
                    else if(this.props.insBuy) {
                        let newFactor = this.props.factor;
                        let newUserdetails = this.props.user_details;
                        newUserdetails.user_id = response.data.data.id;
                        newUserdetails._token = response.data.access_token;

                        const id = response.data.data.id;
                        if(this.props.insurType === 'body') {
                            newFactor.user_id = id;
                        }
                        else if(this.props.insurType === 'Third') {
                            newFactor.user_id = id;
                        }
                        else if(this.props.insurType === 'motor') {
                            newFactor.user_id = id;
                        }
                        else if(this.props.insurType === 'fire') {
                            newFactor.user_id = id;
                        }
                        else if(this.props.insurType === 'complete') {
                            newFactor.user_id = id;
                        }
                        else if(this.props.insurType === 'travel') {
                            newFactor.user_id = id;
                            newFactor.insurance.user_id = id;
                            newFactor.dataSelect.insurance.user_id = id;
                        }
                        else if(this.props.insurType === 'responsible') {
                            newFactor.user_id = id;
                        }
                        else if(this.props.insurType === 'life') {
                            newFactor.user_id = id;
                            newFactor.insurance.user_id = id;
                        }

                        if(response.data.data.wasRecentlyCreated === 0) {
                            Actions.register({openDrawer: this.props.openDrawer, mobile: this.props.mobile, insBuy: true, pageTitle:'مشخصات خریدار', factor:newFactor, user_details:newUserdetails, insurType:this.props.insurType, instalment: this.props.instalment, installmentType: this.props.installmentType})
                        }
                        else {
                            Actions.prices({openDrawer: this.props.openDrawer, insBuy: true, pageTitle:'مشخصات خریدار', factor:newFactor, user_details:newUserdetails, insurType:this.props.insurType, instalment: this.props.instalment, installmentType: this.props.installmentType})
                        }
                    }
                })
            })
            .catch((response) => {
                console.log(response.response)
                if(response.response.data.msg === 'ErorrInput' || response.response.data.msg === 'IncorrectCode'){
                    this.setState({codeDetect: true, modalVisible: true, loading: false});
                }
                else {
                    console.log(response.response)

                    this.setState({modalVisible: true, loading: false});

                }
            });
        }
    }
    resend() {
        this.setState({loading: true});
        Axios.post('/resend_activation', {
            mobile: this.props.mobile
        }).then(response=> {
            this.setState({
                loading: false
            }, () => {
                console.log('confirm data', response);
            })
        })
        .catch((error) => {

            this.setState({modalVisible: true, loading: false});
        });
    }
    closeModal(title) {
        this.setState({modalVisible: false});
        if(title === 'voice'){
            this.setState({loading: true});
        }
    }
    // timerComplete() {
    //     console.log('complete');
    //     this.setState({timerEnd: true});
    // }
    render() {
        const {user} = this.props;
        if(this.state.loading){
            return (<Loader />)
        }
        return (
            <KeyboardAvoidingView style={styles.container}  behavior="padding" enabled>
            {/*<View behavior={'padding'} style={styles.container}>*/}
                {this.state.loading ? <Loader send={false}/> :
                    <View style={styles.send}>
                        <Image style={{alignSelf: 'center', marginRight: 20, width: '40%', resizeMode: 'contain'}} res
                               source={require('../../assets/coffebimeLogo.png')}/>
                        <View style={styles.body}>
                            <Text style={styles.header}>ورود به بیمه کافه</Text>
                            {/*<Text style={styles.label}>برای شماره {this.props.mobile} یک کد فرستاده شده کد را وارد کنید</Text>*/}
                            <View style={{
                                display: 'flex',
                                width: '100%',
                                flexDirection: 'row',
                                justifyContent: 'space-around',
                                alignContent: 'space-between'
                            }}>
                                <TextInput
                                    // maxLength={1}
                                    keyboardType='numeric'
                                    placeholderTextColor={'#C8C8C8'}
                                    underlineColorAndroid='transparent'
                                    style={{
                                        textAlign: 'center',
                                        borderBottomWidth: 1,
                                        borderBottomColor: '#C8C8C8',
                                        height: 45,
                                        backgroundColor: 'white',
                                        paddingRight: 10,
                                        paddingLeft: 10,
                                        flex: .6,
                                        borderRadius: 7,
                                        fontSize: 18,
                                        color: '#7A8299',
                                        marginBottom: 20
                                    }}
                                    value={this.state.code}
                                    onChangeText={(text) => {
                                        this.setState({code: text})

                                    }}/>
                                <TimerCountdown
                                    initialSecondsRemaining={1000*60}
                                    // onTimeElapsed={() => this.timerComplete()}
                                    onTimeElapsed={() => null}
                                    allowFontScaling={true}
                                    style={{ fontSize: 18, paddingTop: 15}}
                                />
                            </View>
                            <TouchableOpacity onPress={() => this.confirm()} style={styles.advertise}>
                                <LinearGradient start={{x: 0, y: 1}} end={{x: 1, y: 1}} colors={['#3cd0ef', '#23d1ba']} style={styles.advertise}>
                                    <View>
                                        <Text style={styles.buttonTitle}>ورود</Text>
                                    </View>
                                </LinearGradient>
                            </TouchableOpacity>
                            {
                                this.state.timerEnd ?
                                    <TouchableOpacity onPress={() => this.setState({modalVisible: true})} style={styles.advertise2}>
                                        <LinearGradient start={{x: 0, y: 1}} end={{x: 1, y: 1}} colors={['rgb(255, 192, 51)', 'rgb(255, 192, 51)']} style={styles.advertise2}>
                                            <View>
                                                <Text style={styles.buttonTitle}>پیامک صوتی</Text>
                                            </View>
                                        </LinearGradient>
                                    </TouchableOpacity>
                                    : null
                            }
                        </View>
                        <View  style={styles.footerContainer}>
                            <View style={styles.footer}>
                                <TouchableOpacity onPress={() => this.resend()}>
                                    <Text style={styles.signup}>ارسال مجدد</Text>
                                </TouchableOpacity>
                                <Text style={styles.footerText}>کد جدید دریافت نکردید؟ / </Text>
                            </View>
                            <TouchableOpacity onPress={() => Actions.push('login')}>
                                <Text style={styles.signup}>تغییر شماره</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                }
                <Image style={{height:'20%',zIndex:-1,width:'100%',position:'absolute',bottom:0}} resizeMode={'cover'} source={require('../../assets/company-hero-3.png')}/>
                <AlertView
                    closeModal={(title) => this.closeModal(title)}
                    timerEnd={true}
                    modalVisible={this.state.modalVisible}
                    onChange={() => this.setState({modalVisible: false})}
                    title={this.state.codeDetect ? 'لطفا کد صحیح را وارد کنید':(this.state.timerEnd ? 'ارسال رمز یکبار مصرف از طریق پیامک صوتی به شما' : 'مشکلی در برقراری ارتباط با سرور به وجود آمده لطفا دوباره سعی کنید')}
                />
            {/*</View>*/}
            </KeyboardAvoidingView>
        );
    }
}
export default Confirmation;