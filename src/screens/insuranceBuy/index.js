import React, {Component} from 'react';
import { View, TouchableOpacity, Text, Linking, ImageBackground ,Alert, Image, BackHandler, NetInfo, Dimensions, AsyncStorage, ScrollView} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux';
import background from '../../assets/bg.png'
import Axios from 'axios';
import homeBG from '../../assets/homeBG6.png';
export const url = 'http://bimecafe.com/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import ProfileItem from "../../components/profileItem/index";
import Icon from 'react-native-vector-icons/dist/Feather';
import MIcon from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import FooterMenu from "../../components/footerMenu/index";
import AlertView from "../../components/modalMassage";
import bime1 from "../../assets/newInsurance/body.png";
import bime2 from "../../assets/newInsurance/third.png";
import bime3 from "../../assets/newInsurance/travel.png";
import bime4 from "../../assets/newInsurance/medical.png";
import bime5 from "../../assets/newInsurance/fire.png";
import bime6 from "../../assets/newInsurance/life.png";
import bime7 from "../../assets/newInsurance/indi.png";
import motor from "../../assets/motor.png";
import medical from "../../assets/medical.png";
import moment_jalaali from "moment-jalaali";

import {connect} from 'react-redux';
import {store} from '../../config/store';
class InsuranceBuy extends Component {
    constructor(props){
        super(props);
        this.state = {
            text: '',
            login: true,
            loading: true,
            modalVisible: false,
            wifi: false,
            isConnected: false,
            logout: true
        };
    }
    componentWillUpdate(){
        BackHandler.addEventListener("hardwareBackPress",  this.onBackPress.bind(this));
    }
    closeModal() {
        this.setState({modalVisible: false});
    }

    componentWillMount() {
        // AsyncStorage.removeItem('token')
        // console.log('is nuuuuumber',/^[-]?\d+$/.test('08555fyfhfghf'))
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
        NetInfo.getConnectionInfo().then((connectionInfo) => {
            console.log('heere is insBuy')
            AsyncStorage.getItem('insuranceInfo').then((info) => {
                if(info !== null) {
                    const newInfo = JSON.parse(info);
                    Linking.getInitialURL().then(url => {
                        console.log('url', url)
                        if(url !== null) {
                            Actions.comment({openDrawer: this.props.openDrawer, modalOpen: true, insTitle: newInfo.title, insId: newInfo.id});
                            console.log('userId', newInfo.user_id)
                            console.log('nnnnnnewinfo', newInfo)
                        }
                    });
                }
                else {
                    Linking.getInitialURL().then(url => {
                        console.log('url', url)
                        if(url !== null) {
                            Actions.insuranceBuy({openDrawer: this.props.openDrawer, modalOpen: false});
                        }
                    });
                }

                AsyncStorage.getItem('token').then((info) => {
                    if(info !== null) {
                        const newInfo = JSON.parse(info);
                        console.log('iiiiiiiiinfo', newInfo)
                        // this.setState({loading: true})
                        Axios.post('/user', {
                            mobile: newInfo.mobile
                        }).then(response=> {
                            this.setState({loading: false});

                            console.log('profile', newInfo.mobile)
                            store.dispatch({type: 'USER_INFO_FETCHED', payload: response.data.data});
                            this.setState({loading: false})
                        })
                            .catch((error) => {
                                // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                                // this.setState({loading: false});
                                this.setState({modalVisible: true, loading: false});
                            });
                    }
                    else {
                        this.setState({loading: false})
                    }
                });

            });

            this.setState({isConnected: connectionInfo.type !== 'none'}, ()=> {
                if(this.state.isConnected === false){
                    this.setState({modalVisible: true, loading: false, wifi: true})
                }
            })
        });

    }

    // componentWillMount() {
    //     BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    //     NetInfo.getConnectionInfo().then((connectionInfo) => {
    //         this.setState({isConnected: connectionInfo.type !== 'none'}, ()=> {
    //             if(this.state.isConnected === false){
    //                 this.setState({modalVisible: true, loading: false, wifi: true})
    //             }
    //         })
    //     });
    //     AsyncStorage.getItem('insuranceInfo').then((info) => {
    //         if(info !== null) {
    //             const newInfo = JSON.parse(info);
    //             Linking.getInitialURL().then(url => {
    //                 console.log('url', url)
    //                 if(url !== null) {
    //                     Actions.comment({openDrawer: this.props.openDrawer, modalOpen: true, insTitle: newInfo.title, insId: newInfo.id});
    //                     console.log('userId', newInfo.user_id)
    //                     console.log('nnnnnnewinfo', newInfo)
    //                 }
    //             });
    //         }
    //         else {
    //             Linking.getInitialURL().then(url => {
    //                 console.log('url', url)
    //                 if(url !== null) {
    //                     Actions.insuranceBuy({openDrawer: this.props.openDrawer, modalOpen: false});
    //                 }
    //             });
    //         }
    //     });
    //
    //
    // }
    // componentDidMount() {
    //     console.log('heere is insBuy in will')
    //     AsyncStorage.getItem('token').then((info) => {
    //         if(info !== null) {
    //             const newInfo = JSON.parse(info);
    //             console.log('iiiiiiiiinfo', newInfo)
    //
    //             // this.setState({loading: true})
    //             Axios.post('/user', {
    //                 mobile: newInfo.mobile
    //
    //             }).then(response=> {
    //                 this.setState({loading: false});
    //                 console.log('profile', newInfo.mobile)
    //                 store.dispatch({type: 'USER_INFO_FETCHED', payload: response.data.data});
    //                 this.setState({loading: false})
    //
    //             })
    //                 .catch((error) => {
    //                     this.setState({modalVisible: true, loading: false});
    //                 });
    //         }
    //         else {
    //             console.log('nothing found and not login')
    //         }
    //     });
    // }
    onBackPress() {
        console.log('some thing loged')
        this.logout()
        return true;
    };
    logout = () => {
        Alert.alert(
            'خروج از اپلیکیشن',
            'آیا از  خروج خود مطمئن هستید؟',
            [
                {text: 'خیر', onPress: () => this.setState({logout: false}), style: 'cancel'},
                {text: 'بله', onPress: () =>  this.setState({logout: true}, () => { BackHandler.exitApp();})},
            ]
        )
    }
    render() {
        const {user} = this.props;
        if(this.state.loading){
            return (<Loader />)
        }
        else return (
            <View style={styles.container}>
                <ScrollView style={styles.scroll}>
                    <ImageBackground source={homeBG} style={styles.image}>
                        <View style={styles.imageRow}>
                            <View style={styles.profile}>
                                {/*<View style={styles.iconContainer}>*/}
                                    {/*<Icon name="user" size={20} />*/}
                                {/*</View>*/}
                                {/*<Text style={styles.name}>{user !== null ? user.fname : ''} {user !== null ? user.lname : ''}</Text>*/}
                            </View>
                            <TouchableOpacity onPress={() => Actions.drawerOpen()}>
                                <Icon name="menu" size={30} color="white" style={{paddingRight: 15}} />
                            </TouchableOpacity>
                        </View>
                        <Image style={{position: 'absolute', top: 20, left: '39%'}}
                               source={require('../../assets/hmLogo.png')}/>
                    </ImageBackground>
                    <View style={styles.body}>
                        <View style={styles.row}>
                            <View style={styles.item}>
                                <TouchableOpacity onPress={() => Actions.otherVacleInfo({openDrawer: this.props.openDrawer, fire: true, pageTitle: 'بیمه آتش سوزی'})} style={styles.imageContainer}>
                                    <Image source={bime5} style={styles.bodyImage}  />
                                </TouchableOpacity>
                                <Text style={styles.label}>بیمه آتش سوزی</Text>
                            </View>
                            <View style={styles.item}>
                                <TouchableOpacity style={styles.imageContainer} onPress={() => Actions.vacleInfo({openDrawer: this.props.openDrawer, bodyBime: true, pageTitle: 'بیمه بدنه خودرو'})}>
                                    <Image source={bime1} style={styles.bodyImage} />
                                </TouchableOpacity>
                                <Text style={styles.label}>بیمه بدنه خودرو</Text>
                            </View>
                            <View style={styles.item}>
                                <TouchableOpacity style={styles.imageContainer} onPress={() => Actions.vacleInfo({openDrawer: this.props.openDrawer, thirdBime: true,motor: false, pageTitle: 'بیمه شخص ثالث'})}>
                                    <Image source={bime2} style={styles.bodyImage} />
                                </TouchableOpacity>
                                <Text style={styles.label}>بیمه شخص ثالث</Text>
                            </View>
                        </View>
                        <View style={styles.row}>
                            <View style={styles.item}>
                                <TouchableOpacity onPress={() => Actions.otherVacleInfo({openDrawer: this.props.openDrawer, individual: true, pageTitle: 'بیمه درمان انفرادی'})} style={styles.imageContainer}>
                                    <Image source={bime7} style={styles.bodyImage} />
                                </TouchableOpacity>
                                <Text style={styles.label}>بیمه درمان انفرادی</Text>
                            </View>
                            <View style={styles.item}>
                                <TouchableOpacity onPress={() => Actions.otherVacleInfo({openDrawer: this.props.openDrawer, medical: true, pageTitle: 'بیمه مسئولیت پزشکی'})} style={styles.imageContainer}>
                                    <Image source={medical} style={styles.bodyImage} />
                                    {/*<MIcon name="medical-bag" color="#2ad1c7" size={40} style={{padding: 5, tintColor: '#2ad1c7'}} />*/}
                                </TouchableOpacity>
                                <Text style={styles.label}>بیمه مسئولیت پزشکان و پیرا پزشکان</Text>
                            </View>
                            <View style={styles.item}>
                                <TouchableOpacity onPress={() => Actions.otherVacleInfo({openDrawer: this.props.openDrawer, age: true, pageTitle: 'بیمه عمر'})} style={styles.imageContainer}>
                                    <Image source={bime6} style={styles.bodyImage} />
                                </TouchableOpacity>
                                <Text style={styles.label}>بیمه عمر</Text>
                            </View>
                        </View>
                        <View style={styles.lastrow}>
                            <View style={styles.item}>
                                <TouchableOpacity onPress={() => Actions.otherVacleInfo({openDrawer: this.props.openDrawer, travel: true, pageTitle: 'بیمه مسافرتی'})}  style={styles.imageContainer}>
                                    <Image source={bime3} style={styles.bodyImage} />
                                </TouchableOpacity>
                                <Text style={styles.label}>بیمه مسافرتی</Text>
                            </View>
                            {/*<View style={styles.item}>*/}
                                {/*<TouchableOpacity style={[styles.imageContainer, { paddingTop: 30, paddingBottom: 30}]} onPress={() => Actions.vacleInfo({openDrawer: this.props.openDrawer, thirdBime: true, motor: true, pageTitle: 'بیمه موتور سیکلت'})}>*/}
                                    {/*<Image source={motor} style={[styles.bodyImage, {tintColor: '#2ad1c7'}]} />*/}
                                {/*</TouchableOpacity>*/}
                                {/*<Text style={styles.label}>بیمه موتور سیکلت</Text>*/}
                            {/*</View>*/}
                            <View style={styles.item}>
                                <TouchableOpacity onPress={() => Actions.otherVacleInfo({openDrawer: this.props.openDrawer, accident: true, pageTitle: 'بیمه حوادث انفرادی'})} style={styles.imageContainer}>
                                    <Image source={bime4} style={styles.bodyImage} />
                                </TouchableOpacity>
                                <Text style={styles.label}>بیمه حوادث انفرادی</Text>
                            </View>
                            {/*<View style={styles.item}>*/}
                                {/*<View style={styles.imageContainer}>*/}
                                    {/*<Image source={bime7} style={styles.bodyImage} />*/}
                                {/*</View>*/}
                                {/*<Text style={styles.label}>بیمه مسئولیت مشاغل</Text>*/}
                            {/*</View>*/}
                        </View>
                        <AlertView
                            closeModal={(title) => this.closeModal(title)}
                            modalVisible={this.state.modalVisible}
                            onChange={() => this.setState({modalVisible: false})}
                            title={(this.state.wifi ? 'لطفا وضعیت وصل بودن به اینترنت را بررسی کنید': 'مشکلی در برقراری ارتباط با سرور به وجود آمده لطفا دوباره سعی کنید')}
                        />
                    </View>
                </ScrollView>
                <FooterMenu active="bime" openDrawer={this.props.openDrawer}/>
                <View style={[styles.TrapezoidStyle, {borderRightWidth: Dimensions.get('window').width}]} />
            </View>
        );
    }
}
// export default InsuranceBuy;
function mapStateToProps(state) {
    return {
        user: state.auth.user,
    }
}
export default connect(mapStateToProps)(InsuranceBuy);