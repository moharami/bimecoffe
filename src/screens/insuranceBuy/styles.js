import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
        position: 'relative'
    },
    image: {
        width: '100%',
        height: '32%'
    },
    imageRow: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'space-between',
        paddingTop: 20
    },
    profileImage: {
        width: 85,
        height: 85,
        borderRadius: 85,
    },
    profile: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: 30,
        paddingBottom: 7
    },
    name: {
        fontSize: 13,
        color: 'rgb(255, 255, 255)',
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'center',
        paddingTop: 10,
        paddingLeft: 20,
        paddingBottom: 5
    },
    item: {
        width: '28%',
        height: '60%',
    },
    imageContainer: {
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 20,
        borderRadius: 100,
        // elevation: 8,
        borderWidth: 2,
        borderTopColor: '#38d0e6',
        borderRightColor: '#38d0e6',
        borderLeftColor: '#24d1ba',
        borderBottomColor: '#24d1ba',
    },
    bodyImage: {
        width: 40,
        resizeMode: 'contain',
        tintColor: '#2ad1c7'
    },
    bodyyImage: {
        width: 40,
        resizeMode: 'contain',
        tintColor: '#2ad1c7'
    },
    label: {
        fontSize: 12,
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'center',
        paddingTop: 10,
        color: 'rgb(60, 60, 60)'
    },
    scroll: {
        flex: 1,
        marginBottom: 80
    },
    body : {
        alignItems: 'center',
        justifyContent: 'flex-start',
        paddingRight: 20,
        paddingLeft: 20,
        paddingBottom: '53%',
        paddingTop: 2
    },
    row: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        height: 140
    },
    lastrow: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
    },
    iconContainer: {
        width: 30,
        height: 30,
        borderRadius: 30,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    }
});
